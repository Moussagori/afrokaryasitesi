using System;
using System.Collections.Generic;

namespace AfrokaryaTicaret.Models
{
    public partial class UrunOzellik
    {
        public int UrunID { get; set; }
        public int OzellikTipiID { get; set; }
        public int OzellikDegerID { get; set; }
        public virtual OzellikDeger OzellikDeger { get; set; }
        public virtual OzellikTip OzellikTip { get; set; }
        public virtual Urun Urun { get; set; }
    }
}
