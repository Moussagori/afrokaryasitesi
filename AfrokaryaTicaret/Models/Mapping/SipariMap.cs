using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AfrokaryaTicaret.Models.Mapping
{
    public class SipariMap : EntityTypeConfiguration<Sipari>
    {
        public SipariMap()
        {
            // Primary Key
            this.HasKey(t => t.İd);

            // Properties
            this.Property(t => t.Adi)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Aciklma)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Siparis");
            this.Property(t => t.İd).HasColumnName("İd");
            this.Property(t => t.Adi).HasColumnName("Adi");
            this.Property(t => t.Aciklma).HasColumnName("Aciklma");
        }
    }
}
