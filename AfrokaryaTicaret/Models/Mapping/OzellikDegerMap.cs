using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace AfrokaryaTicaret.Models.Mapping
{
    public class OzellikDegerMap : EntityTypeConfiguration<OzellikDeger>
    {
        public OzellikDegerMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.Adi)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Aciklma)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("OzellikDeger");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.Adi).HasColumnName("Adi");
            this.Property(t => t.Aciklma).HasColumnName("Aciklma");
            this.Property(t => t.OzellikTipiID).HasColumnName("OzellikTipiID");

            // Relationships
            this.HasRequired(t => t.OzellikTip)
                .WithMany(t => t.OzellikDegers)
                .HasForeignKey(d => d.OzellikTipiID);

        }
    }
}
