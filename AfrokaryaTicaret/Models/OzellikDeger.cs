using System;
using System.Collections.Generic;

namespace AfrokaryaTicaret.Models
{
    public partial class OzellikDeger
    {
        public OzellikDeger()
        {
            this.UrunOzelliks = new List<UrunOzellik>();
        }

        public int id { get; set; }
        public string Adi { get; set; }
        public string Aciklma { get; set; }
        public int OzellikTipiID { get; set; }
        public virtual OzellikTip OzellikTip { get; set; }
        public virtual ICollection<UrunOzellik> UrunOzelliks { get; set; }
    }
}
