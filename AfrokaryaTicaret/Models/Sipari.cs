using System;
using System.Collections.Generic;

namespace AfrokaryaTicaret.Models
{
    public partial class Sipari
    {
        public Sipari()
        {
            this.Satis = new List<Sati>();
        }

        public int İd { get; set; }
        public string Adi { get; set; }
        public string Aciklma { get; set; }
        public virtual ICollection<Sati> Satis { get; set; }
    }
}
