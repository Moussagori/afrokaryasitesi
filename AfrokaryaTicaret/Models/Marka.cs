using System;
using System.Collections.Generic;

namespace AfrokaryaTicaret.Models
{
    public partial class Marka
    {
        public Marka()
        {
            this.Uruns = new List<Urun>();
        }

        public int id { get; set; }
        public string Adi { get; set; }
        public string Aciklma { get; set; }
        public Nullable<int> ResimID { get; set; }
        public virtual Resim Resim { get; set; }
        public virtual ICollection<Urun> Uruns { get; set; }
    }
}
